<?php


class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}



	public function index()
	{
		$this->load->model('project_model');
		$projects = $this->project_model->show_all_projects();
		$this->load->model('heading_model');
		$headings = $this->heading_model->show_heading();
		$this->load->view('user/index', ['headings'=>$headings, 'projects'=>$projects]);
	}




	public function store_message()
	{
		$this->load->helper('date');
		$date = '%M %d %Y';
		// $day = '%l';
		$time = time();
		$today_date =  mdate($date, $time);
		// $today_day = mdate($day, $time);

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('mobile', 'Mobile no', 'required');
		$this->form_validation->set_rules('message', 'Message', 'required');

		if( $this->form_validation->run() )
		{
			$message_data = array(
			'name'		=>		$this->input->post('name'),
			'email'		=>		$this->input->post('email'),
			'mobile'	=>		$this->input->post('mobile'),
			'message'	=>		$this->input->post('message'),
			'date'		=>		$today_date
			);

			$this->load->model('message_model');
			if( $this->message_model->store_message($message_data) )
			{
				$this->session->set_flashdata('msg', 'Your Message was successfully send. We will respond you soon. Thank You..!');
				return redirect('home/index#contact');
			}
		}
		else{
			$this->session->set_flashdata('msg', 'Please fill all the fields..!');
			return redirect('home/index#contact');
		}
	}



























} // Home Controller End