<?php

class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if( ! $this->session->userdata('set_admin'))
			return redirect('login/index');

		$this->load->library('form_validation');
		$this->load->model('project_model');
	}





	public function index()
	{
		$this->load->model('message_model');
		$messages = $this->message_model->show_messages();
		$this->load->view('admin/index', ['messages'=>$messages]);
	}


	public function logout()
	{
		$this->session->unset_userdata('set_admin');
		return redirect('login/index');
	}


	// HEADING FUNCTIONS START
	public function add_heading()
	{
		$this->load->view('admin/heading_add');
	}

	public function store_heading()
	{
		$this->form_validation->set_rules('heading', 'Heading', 'required');
		$this->form_validation->set_rules('order', 'Order No', 'required');

		if ($this->form_validation->run() == FALSE)
        {
               $this->load->view('admin/heading_add');
        }
        else
        {
                $data = $this->input->post();
				$this->load->model('heading_model');
				$heading = $this->heading_model->store_heading($data);

				if( $heading ){
					return redirect('admin/show_heading');
				}
				else{
					echo "Error at Database";
				}
        }

		
	}

	public function show_heading()
	{
		$this->load->model('heading_model');
		$headings = $this->heading_model->show_heading();
		$this->load->view('admin/heading_show', ['headings'=>$headings]);
	}

	public function edit_heading($heading_id)
	{
		$this->load->model('heading_model');
		$heading = $this->heading_model->edit_heading($heading_id);
		$this->load->view('admin/heading_edit', ['headings'=>$heading]);
	}

	public function update_heading()
	{
		$id = $this->input->post('id');
		$data = $this->input->post();
		$this->load->model('heading_model');
		$heading = $this->heading_model->update_heading($id, $data);
		if( $heading ){
			return redirect('admin/show_heading');
		}
		else{
			echo "Error";
		}
	}

	public function del_heading()
	{
		$heading_id = $this->input->post('heading_id');
		$this->load->model('heading_model');
		$delete_status = $this->heading_model->del_heading($heading_id);
		if($delete_status){
			return redirect('admin/show_heading');
		}
	}
	// HEADING FUNCTIONS END










	// PROJECTS FUNCTIONS START
	public function add_project()
	{
		$options = array(
	        'exterior'    		=> 'Exterior Designs',
	        'interior'  				=> 'Interior & Furniture Designs',
	        'logo' 			=> 'Brand & Identity Design',
	        'digital'  			=> 'Digital Paintings',
	        'pencil' 			=> 'Pencil Sketches',
		);
		$this->load->view('admin/project_add', ['options'=>$options]);
	}

	public function store_project()
	{
		$config = [
				'upload_path'		=>		'./assets/projects/',
				'allowed_types'		=>		'jpg|jpeg|png',
				'max_size'			=>		'0'
		];
		$this->load->library('upload', $config);
		if( $this->upload->do_upload('project_image') ){
			$data = $this->upload->data();
			$project_data = array(
						'project_category' => $this->input->post('project_category'),
						'project_image' => base_url("assets/projects/" . $data['raw_name'] . $data['file_ext']),
					);
			
			$projects = $this->project_model->add_project($project_data);
			if( $projects )
			{
				return redirect('admin/show_projects');
			}
			else{
				echo "Failed";
			}
		}
	}

	public function show_projects()
	{
		$this->load->model('project_model');
		$exterior_designes = $this->project_model->show_exterior();
		$interior_designes = $this->project_model->show_interior();
		$logo_designes = $this->project_model->show_logo();
		$digital_designes = $this->project_model->show_digital();
		$pencil_designes = $this->project_model->show_pencil();

		$designs_cat = array(
			'exterior_designes'=>$exterior_designes,
			'interior_designes'=>$interior_designes,
			'logo_designes'=>$logo_designes, 
			'digital_designes'=>$digital_designes,
			'pencil_designes'=>$pencil_designes
		);
		$this->load->view('admin/project_show', $designs_cat);
	}

	public function del_project()
	{
		$project_id = $this->input->post('project_id');
		if( $this->project_model->del_project($project_id) )
		{
			return redirect('admin/show_projects');
		}
		else
		{
			echo "Something Wents Wrong";
		}
	}
	// PROJECT FUNCTION END




	public function message()
	{
		$this->load->model('message_model');
		$messages = $this->message_model->show_messages();
		$this->load->view('admin/message', ['messages'=>$messages]);
	}

	public function del_msg()
	{
		$msg_id = $this->input->post('msg_id');
		$this->load->model('message_model');
		if( $this->message_model->del_msg($msg_id) ){
			return redirect('admin/message');
		}
	}



}