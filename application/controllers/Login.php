<?php

/**
* 
*/
class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if( $this->session->userdata('set_admin'))
			return redirect('admin');
	}

	public function index()
	{
		$this->load->view('admin/login');
	}


	public function verify_admin()
	{
		echo $username = $this->input->post('username');
		echo $password = $this->input->post('password');

		$this->load->model('login_model');
		$set_admin = $this->login_model->verify($username, $password);

		// echo "<pre>";
		// print_r($set_admin);

		if( $set_admin )
		{
			$this->session->set_userdata('set_admin', $set_admin);
			return redirect('admin/index');
		}
		else{
			return redirect('login/index');
		}
	}




}