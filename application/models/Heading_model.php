<?php


class Heading_model extends CI_Model
{

	public function store_heading($heading)
	{
		$add_query = $this->db->insert('heading', $heading);

		if( count($add_query) > 0 ) {
			return TRUE;
		}
	}


	public function show_heading()
	{
		$show_heading = $this->db->get('heading');
		return $show_heading->result();
	}

	public function edit_heading($heading_id)
	{
		$find = $this->db->where('id', $heading_id)
						 ->get('heading');

		return $find->row();
	}

	public function update_heading($id, $data)
	{
		$update = $this->db->update('heading', $data, array('id' => $id));
		if( count($update) > 0 ) {
			return TRUE;
		}
	}

	public function del_heading($heading_id)
	{
		$del_heading = $this->db->delete('heading', ['id'=>$heading_id]);
		if( count($del_heading) > 0 ) {
			return TRUE;
		}
	}



}