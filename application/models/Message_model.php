<?php


class Message_model extends CI_Model
{
	
	public function store_message($data)
	{
		$store_data = $this->db->insert('message', $data);
		if( count($store_data) > 0 )
		{
			return TRUE;
		}
	}

	public function show_messages()
	{
		$show_messages = $this->db->get('message');
		return $show_messages->result();
	}

	public function del_msg($msg_id)
	{
		$del_message = $this->db->delete('message', ['id'=>$msg_id]);
		if( count($del_message) > 0 ){
			return TRUE;
		}
	}
}