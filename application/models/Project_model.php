<?php

class Project_model extends CI_Model
{

	public function add_project($project_data)
	{
		$add_project = $this->db->insert('projects', $project_data);
		if( count($add_project) > 0 )
		{
			return TRUE;
		}
	}

	public function show_all_projects()
	{
		$show_projects = $this->db->get('projects');
		return $show_projects->result();
	}


	public function show_exterior()
	{
		$find_exterior = $this->db->where('project_category', 'exterior')
						 ->get('projects');
		return $find_exterior->result();
	}

	public function show_interior()
	{
		$find_interior = $this->db->where('project_category', 'interior')
						 ->get('projects');
		return $find_interior->result();
	}

	public function show_logo()
	{
		$find_logo = $this->db->where('project_category', 'logo')
						 ->get('projects');
		return $find_logo->result();
	}

	public function show_digital()
	{
		$find_digital = $this->db->where('project_category', 'digital')
						 ->get('projects');
		return $find_digital->result();
	}

	public function show_pencil()
	{
		$find_pencil = $this->db->where('project_category', 'pencil')
						 ->get('projects');
		return $find_pencil->result();
	}


	public function del_project($project_id)
	{
		$del_project = $this->db->delete('projects', ['id'=>$project_id]);
		$row = $this->db->where('id',$project_id)->get('projects')->row();
		unlink('assets/projects/'.$row->project_image);
		if(count($del_project) > 0)
		{
			return TRUE;
		}
	}
	

}