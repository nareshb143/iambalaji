        


        </div><!-- d-flex -->

        <!-- // END drawer-layout -->
        </div><!-- mdk-drawer-layout -->
        

        <!-- jQuery -->
        <script type="text/javascript" src="<?= base_url('assets/admin/vendor/jquery.min.js') ?>"></script>

        <!-- Bootstrap -->
        <script type="text/javascript" src="<?= base_url('assets/admin/vendor/popper.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/admin/vendor/bootstrap.min.js') ?>"></script>

        <script type="text/javascript" src="<?= base_url('assets/admin/vendor/Chart.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/admin/vendor/moment.min.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/admin/vendor/dateformat.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/admin/vendor/bootstrap-datepicker.min.js') ?>"></script>

        <script>
            window.theme = "default";
        </script>
        <script type="text/javascript" src="<?= base_url('assets/admin/js/color_variables.js') ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/admin/js/app.js') ?>"></script>


        <script type="text/javascript" src="<?= base_url('assets/admin/vendor/dom-factory.js') ?>"></script>
        <!-- DOM Factory -->
        <script type="text/javascript" src="<?= base_url('assets/admin/vendor/material-design-kit.js') ?>"></script>
        <!-- MDK -->



        <script>
            (function() {
                'use strict';

                // Self Initialize DOM Factory Components
                domFactory.handler.autoInit()

                // Connect button(s) to drawer(s)
                var sidebarToggle = Array.prototype.slice.call(document.querySelectorAll('[data-toggle="sidebar"]'))

                sidebarToggle.forEach(function(toggle) {
                    toggle.addEventListener('click', function(e) {
                        var selector = e.currentTarget.getAttribute('data-target') || '#default-drawer'
                        var drawer = document.querySelector(selector)
                        if (drawer) {
                            drawer.mdkDrawer.toggle()
                        }
                    })
                })

                //////////////////////////////////////////
                // BREAK OUT OF ENVATO LIVE DEMO IFRAME //
                //////////////////////////////////////////

                window.top.location.hostname !== window.location.hostname && (window.top.location = window.location)

            })();
        </script>



        <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="defaultModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>



        <!-- Delete heading Popup -->
        <div class="modal fade" id="deleteHeading" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="defaultModalLabel">Delete heading</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        Are you sure you want to delete this heading ?
                    </div>
                    <div class="modal-footer">
                        <?=
                            form_open('admin/del_heading'),
                            form_input(array('name' => 'heading_id', 'type'=>'hidden', 'id' =>'heading_id')),
                            form_submit(['name'=>'submit', 'value'=>'Delete', 'class'=>'btn btn-outline-danger del_heading', 'data-toggle'=>'modal', 'data-dismiss'=>'model']),
                            form_close();
                        ?>
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Delete heading Popup END -->


        <!-- Delete Project Popup -->
        <div class="modal fade" id="deleteProject" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="defaultModalLabel">Delete Project</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        Are you sure you want to delete this project ?
                    </div>
                    <div class="modal-footer del_project_form">
                        <?=
                            form_open('admin/del_project'),
                            form_input(array('name' => 'project_id', 'type'=>'hidden', 'id' =>'project_id')),
                            form_submit(['name'=>'submit', 'value'=>'Delete', 'class'=>'btn btn-outline-danger del_project', 'data-toggle'=>'modal', 'data-dismiss'=>'model']),
                            form_close();
                        ?>
                        <button type="button" class="btn btn-outline-primary can_project" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Delete Project Popup END -->


        <!-- Delete Message Popup -->
        <div class="modal fade" id="deleteMessage" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="defaultModalLabel">Delete Message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center">
                        Are you sure you want to delete this project ?
                    </div>
                    <div class="modal-footer del_project_form">
                        <?=
                            form_open('admin/del_msg'),
                            form_input(array('name' => 'msg_id', 'type'=>'hidden', 'id' =>'msg_id')),
                            form_submit(['name'=>'submit', 'value'=>'Delete', 'class'=>'btn btn-outline-danger del_project', 'data-toggle'=>'modal', 'data-dismiss'=>'model']),
                            form_close();
                        ?>
                        <button type="button" class="btn btn-outline-primary can_project" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Delete Message Popup END -->
        


       







        <script src="<?= base_url('assets/admin/vendor/fullcalendar.min.js') ?>"></script>
        <script src="<?= base_url('assets/admin/js/calendars.js') ?>"></script>
        <script src="<?= base_url('assets/admin/js/charts_index.js') ?>"></script>

        


</body>


</html>