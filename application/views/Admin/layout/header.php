<!DOCTYPE html>
<html lang="en" dir="ltr" class="theme-default">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admins Panel</title>
    <?= link_tag('assets/user/img/favicon.png', 'icon', 'image/png'); ?>
    <script src="<?=base_url('assets/admin/js/jquery.min.js')?>"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <?= link_tag('assets/admin/css/themes/default/vendor-fullcalendar.css'); ?>
    <?= link_tag('assets/admin/css/themes/default/vendor-weathericons.css'); ?>
    <?= link_tag('assets/admin/css/themes/default/vendor-bootstrap-datepicker.css'); ?>
    <?= link_tag('assets/css/font-awesome.min.css'); ?>

    <!-- App CSS -->
    <?= link_tag('assets/admin/css/themes/default/app.css'); ?>
    <?= link_tag('assets/admin/css/admin_style.css'); ?>
    <?= link_tag('assets/admin/css/style.css'); ?>

</head>
<style>
    .message-count{display: none;}
</style>
<body>
    <div class="d-flex flex-column position-relative h-100"><!-- End in admins_footer -->

        <nav class="navbar navbar-expand-md navbar-light ">
            <!-- END DEMO  COLORS -->
            <button class="navbar-toggler navbar-toggler-right d-md-none d-lg-none" type="button" data-toggle="sidebar">
            <span class="navbar-toggler-icon"></span>
          </button>


            <div class="collapse navbar-collapse" id="mainNavbar">
                <ul class="navbar-nav ml-auto ">
                <li class="nav-item  align-self-center message-count">
                        <div class="nav-link">
                            <div class="navbar--stats">
                                <div class="navbar--stats--icon">
                                    <i class="material-icons align-middle">email</i>
                                </div>
                                <div class="navbar--stats--text align-self-center">
                                    <a href="#"><?php echo count($messages) ?></a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item  align-self-center">
                        <div class="nav-link">
                            <div class="navbar--stats">
                                <div class="navbar--stats--icon">
                                    <i class="material-icons align-middle">exit_to_app</i>
                                </div>
                                <div class="navbar--stats--text align-self-center">
                                    <a href="http://localhost/projects/iambalaji/" target="_black">Iambalaji</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item  align-self-center">
                        <div class="nav-link">
                            <div class="navbar--stats">
                                <div class="navbar--stats--icon">
                                    <i class="material-icons align-middle">power_settings_new</i>
                                </div>
                                <div class="navbar--stats--text align-self-center">
                                    <a href="<?= base_url("admin/logout") ?>">Log out</a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                </div>
        </nav>









        <div class="mdk-drawer-layout js-mdk-drawer-layout" fullbleed push has-scrolling-region>  <!-- End in admins_footer -->
           



            <div class="mdk-drawer-layout__content mdk-header-layout__content--scrollable"><!-- End in admins_sidebar -->
                
                <!-- CONTENT BODY -->
                 <div class="container-fluid"><!-- End in admins_sidebar -->