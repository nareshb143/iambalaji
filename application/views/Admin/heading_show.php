<?php include('layout/header.php'); ?>








<div class="card">
    <div class="card-header d-flex align-items-center justify-content-between">
        <h4 class="card-title">Headings</h4>
        <button class="btn btn-faded btn-sm dropdown-toggle dropdown-no-caret" type="button" id="ordersFilter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="material-icons align-bottom text-muted md-18">filter_list</i>
          <span>Filter</span>
        </button>
        <div class="dropdown-menu" aria-labelledby="ordersFilter">
            <h6 class="dropdown-header">Sort By</h6>
            <a class="dropdown-item" href="#">#</a>
            <a class="dropdown-item" href="#">Date</a>
            <a class="dropdown-item" href="#">Status</a>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table mb-0">
            <thead class="thead-light">
                <tr>
                    <th width="60">ORDER</th>
                    <th class="text-uppercase">Heading</th>
                    <th class="text-center text-uppercase" width="80">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($headings as $heading): ?>
                <tr>
                    <td class="align-middle text-center font-weight-normal"><?php echo $heading->order ?></td>
                    <td class="align-middle"><?php echo $heading->heading ?></td>
                    <td class="text-center align-middle">
                        <div class="d-flex">
                            <?= anchor("admin/edit_heading/{$heading->id}", '<button class="btn btn-white btn-sm mr-1"><i class="material-icons md-14 align-middle">edit</i></button>');?>
                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteHeading" onclick="set_heading_id(<?php echo $heading->id ?>)">
                                <i class="material-icons md-14 align-middle">delete</i>
                            </button>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>





<script>
  function set_heading_id($heading_id)
  {
        $("#heading_id").val($heading_id);
  }
</script>





<?php include('layout/sidebar.php'); ?>





<?php include('layout/footer.php'); ?>