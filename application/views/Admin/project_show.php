<?php include('layout/header.php'); ?>





<div class="card">
    <div class="card-header d-flex align-items-center justify-content-between">
        <h4 class="card-title">Projects</h4>
        <a href="#" class="dropdown-toggle dropdown-no-caret" id="postSettings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="material-icons align-middle text-muted">more_horiz</i>
		</a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="postSettings">
            <a class="dropdown-item" href="#">Hide Post</a>
            <a class="dropdown-item" href="#">Unfollow Karen Smith</a>
            <a class="dropdown-item" href="#">Unfriend Karen Smith</a>
        </div>
    </div>
    <div class="card-body">


        <h4 class="card-title">Exterior Designs</h4>
        <hr>
        <div class="row px-2">
            <?php foreach ($exterior_designes as $design): ?>
            <div class="col-2 p-1">
                <a href="#">
                    <img class="img-fluid" src="<?php echo $design->project_image; ?>" alt="Card image" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php echo $design->project_category; ?>">
                </a>
                <button class="btn btn-danger btn-sm del_project" data-toggle="modal" data-target="#deleteProject" onclick="set_project_id(<?php echo $design->id ?>)">
                    <i class="material-icons md-14 align-middle">delete</i>
                </button>
            </div>
             <?php endforeach; ?>
        </div>
        <hr>

        <h4 class="card-title">Interior & Furniture Designs</h4>
        <hr>
        <div class="row px-2">
            <?php foreach ($interior_designes as $design): ?>
            <div class="col-2 p-1">
                <a href="#">
                    <img class="img-fluid" src="<?php echo $design->project_image; ?>" alt="Card image" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php echo $design->project_category; ?>">
                </a>
                <button class="btn btn-danger btn-sm del_project" data-toggle="modal" data-target="#deleteProject" onclick="set_project_id(<?php echo $design->id ?>)">
                    <i class="material-icons md-14 align-middle">delete</i>
                </button>
            </div>
             <?php endforeach; ?>
        </div>
        <hr>

        <h4 class="card-title">Brand & Identity Design</h4>
        <hr>
        <div class="row px-2">
            <?php foreach ($logo_designes as $design): ?>
            <div class="col-2 p-1">
                <a href="#">
                    <img class="img-fluid" src="<?php echo $design->project_image; ?>" alt="Card image" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php echo $design->project_category; ?>">
                </a>
                <button class="btn btn-danger btn-sm del_project" data-toggle="modal" data-target="#deleteProject" onclick="set_project_id(<?php echo $design->id ?>)">
                    <i class="material-icons md-14 align-middle">delete</i>
                </button>
            </div>
             <?php endforeach; ?>
        </div>
        <hr>

        <h4 class="card-title">Digital Designs</h4>
        <hr>
        <div class="row px-2">
            <?php foreach ($digital_designes as $design): ?>
            <div class="col-2 p-1">
                <a href="#">
                    <img class="img-fluid" src="<?php echo $design->project_image; ?>" alt="Card image" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php echo $design->project_category; ?>">
                </a>
                <button class="btn btn-danger btn-sm del_project" data-toggle="modal" data-target="#deleteProject" onclick="set_project_id(<?php echo $design->id ?>)">
                    <i class="material-icons md-14 align-middle">delete</i>
                </button>
            </div>
             <?php endforeach; ?>
        </div>
        <hr>

        <h4 class="card-title">Pencil Designs</h4>
        <hr>
        <div class="row px-2">
            <?php foreach ($pencil_designes as $design): ?>
            <div class="col-2 p-1">
                <a href="#">
                    <img class="img-fluid" src="<?php echo $design->project_image; ?>" alt="Card image" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php echo $design->project_category; ?>">
                </a>
                <button class="btn btn-danger btn-sm del_project" data-toggle="modal" data-target="#deleteProject" onclick="set_project_id(<?php echo $design->id ?>)">
                    <i class="material-icons md-14 align-middle">delete</i>
                </button>
            </div>
             <?php endforeach; ?>
        </div>
        <hr>

    </div>
</div>


<script>
  function set_project_id($project_id)
  {
        $("#project_id").val($project_id);
  }
</script>



<?php include('layout/sidebar.php'); ?>





<?php include('layout/footer.php'); ?>