<?php include('layout/header.php'); ?>
<style>
    .message-count{display: block !important;}
</style>




<?php foreach( $messages as $message ): ?>
<div class="card">
    <div class="card-body">
        <div class="media align-items-center">
            <div class="media-body">
                <p class="card-text mb-1">
                    <i class="material-icons align-middle ml-2 text-primary">account_box</i> <?php echo $message->name ?>
                    <i class="material-icons align-middle ml-2 text-primary">email</i> <?php echo $message->email ?>
                    <i class="material-icons align-middle ml-2 text-primary">call</i> <?php echo $message->mobile ?>
                    <i class="material-icons align-middle ml-2 text-primary">date_range</i> <?php echo $message->date ?>
                </p>
            </div>
            <a href="#" class="dropdown-toggle dropdown-no-caret" id="postSettings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            	<i class="material-icons align-middle text-muted">more_horiz</i>
          </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="postSettings">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#deleteMessage" onclick="set_msg_id(<?php echo $message->id ?>)"><i class="material-icons align-middle mr-2 text-primary text-danger">account_box</i> Delete</a>
                <a class="dropdown-item" href="#"><i class="material-icons align-middle mr-2 text-primary save">save</i> Save</a>
            </div>
        </div>
        <hr>
        <p class="card-text">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore aliquam eaque accusamus, maxime ex magni assumenda veniam at iste consectetur earum suscipit, praesentium aperiam? Eaque minus aspernatur ipsa suscipit veniam.
        </p>
    </div>
</div>
<?php endforeach; ?>


<script>
  function set_msg_id($msg_id)
  {
        $("#msg_id").val($msg_id);
  }
</script>



<?php include('layout/sidebar.php'); ?>





<?php include('layout/footer.php'); ?>