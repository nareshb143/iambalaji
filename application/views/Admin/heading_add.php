<?php include('layout/header.php'); ?>








<div class="card">
    <div class="card-header">
        <h4 class="card-title">Add Heading</h4>
    </div>
    <div class="card-body">
        <form action="store_heading" method="POST" class="container was-validated" id="needs-validation" novalidate="">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="validationCustom01">Enter Heading</label>
                    <input type="text" name="heading" class="form-control" id="validationCustom01" placeholder="Ex: Professional Photographer" value="" required="">
                    <?php echo form_error('heading', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="validationCustom01">Order</label>
                    <input type="number" style="width: 160px;" name="order" class="form-control" id="validationCustom01" placeholder="Ex: 1" value="" required="">
                    <?php echo form_error('order', '<div class="invalid-feedback">', '</div>'); ?>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Save Heading</button>
        </form>
    </div>
    
</div>











<?php include('layout/sidebar.php'); ?>





<?php include('layout/footer.php'); ?>