<?php include('layout/header.php'); ?>








<div class="card">
    <div class="card-header">
        <h4 class="card-title">Edit Heading</h4>
    </div>
    <div class="card-body">
        <?php echo form_open('admin/update_heading', ['class'=>'container was-validated', 'id'=>'needs-validation', 'novalidate'=>'']); ?>
            <?php echo form_hidden('id', $headings->id); ?>
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="validationCustom01">Enter Heading</label>
                    <?php echo form_input(['name'=>'heading', 'class'=>'form-control', 'id'=>'validationCustom01', 'placeholder'=>'Ex: Professional Photographer', 'required'=>'', 'value'=>set_value('heading', $headings->heading)]); ?>
                </div>
                <div class="col-md-2 mb-3">
                    <label for="validationCustom01">Order</label>
                    <?php echo form_input(['type'=>'number', 'name'=>'order', 'class'=>'form-control', 'id'=>'validationCustom01', 'placeholder'=>'Ex: 1', 'required'=>'', 'value'=>set_value('heading', $headings->order)]); ?>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Update Heading</button>
        </form>
    </div>
    
</div>











<?php include('layout/sidebar.php'); ?>





<?php include('layout/footer.php'); ?>