<?php include('layout/header.php'); ?>









<div class="card">
    <div class="card-header">
        <h4 class="card-title">Add Project</h4>
    </div>
</div>

 <form enctype="multipart/form-data" action="store_project" method="post">
	<div class="card">
	    <div class="card-header">
	        <h4 class="card-title">Select Category</h4>
	    </div>
	    <div class="card-body">
			<?php echo form_dropdown('project_category', $options, 'exterior',['class'=>'custom-select', 'id'=>'project_category']); ?>
	    </div>
	    <div class="card-header">
	        <h4 class="card-title">Select Image</h4>
	    </div>
	    <div class="card-body">
	        <div class="row">
	            <div class="col example-grid-col">
	                <label class="custom-file">
						<?php echo form_upload(['name'=>'project_image', 'id'=>'file2', 'class'=>'custom-file-input']); ?>
						<span class="custom-file-control"></span>
					</label>
	            </div>
	            <div class="col example-grid-col">
	                <img id="image_preview" src="#" alt="iambalaji" style="width: 200px;height: auto;" />
	            </div>
	        </div>
	    </div>
	    <div class="card-body">
	        <input class="btn btn-outline-primary" type="submit" name="fileSubmit" value="Add Project"/>
	        <?php echo form_reset(['name'=>'reset', 'value'=>'Reset', 'class'=>'btn btn-outline-danger']); ?>
	    </div>
	</div>
</form>















<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>



	
<script>

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#image_preview').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#file2").change(function() {
  readURL(this);
});

</script>






<?php include('layout/sidebar.php'); ?>





<?php include('layout/footer.php'); ?>