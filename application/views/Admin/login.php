
<!DOCTYPE html>
<html lang="en" dir="ltr" class="theme-default">



<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admin Login</title>
    <?= link_tag('assets/admin/css/themes/default/app.css'); ?>
    <?= link_tag('assets/user/img/favicon.png', 'icon', 'image/png'); ?>
</head>

<body>

    <div class="container h-vh d-flex justify-content-center align-items-center flex-column">
        <div class="d-flex justify-content-center align-items-center mb-3">
            <span>
                <img src="<?=base_url('assets/user/img/iamb.png')?>" style="width: 200px" alt="iambalaji">
            </span>
        </div>
        <div class="row w-100 justify-content-center">
            <div class="card w-50 mb-3">
                <div class="card-body">
                    <?php echo form_open('login/verify_admin') ?>
                        <div class="form-group">
                            <label for="username">Username</label>

                            <div class="input-group input-group--inline">
                                <div class="input-group-addon">
                                    <i class="material-icons">account_circle</i>
                                </div>
                                <input type="text" class="form-control" name="username" value="iambalajib">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="d-flex">
                                <label for="username">Password</label>
                            </div>

                            <div class="input-group input-group--inline">
                                <div class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </div>
                                <input type="password" class="form-control" name="password" value="">
                            </div>
                        </div>
                        <div class="text-center">
                            <input type="submit" class="btn btn-primary" value="Login">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</body>

</html>