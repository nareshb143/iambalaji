<?php include('layout/header.php'); ?>



<style>
    .slider-area#home{
        background: rgba(0, 0, 0, 0) url(<?=base_url('assets/user/img/bg/large.jpg')?>) no-repeat scroll center bottom 100% / cover;
    }
</style>





<!-- Content area start  -->
        <div class="content">
            <!-- Slider area start -->
            <div class="slider-area" id="home">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="slider-content-wrapper">
                                <div class="slider-content text-center">
                                    <h3>Hello,</h3>
                                    <h1 class="cd-headline clip is-full-width">
                                        <span>I’m </span>
                                        <span class="cd-words-wrapper">
                                            <b class="is-visible">Balaji Battulwar</b>
                                            <?php foreach ($headings as $heading): ?>
                                            <b><?php echo $heading->heading ?></b>
                                            <?php endforeach; ?>
                                        </span>
                                    </h1>
                                    <h1 class="mobile-slider-title">I’m Balaji Battulwar</h1>
                                    <ul class="profile-description">
                                        <li>DESIGNER</li>
                                        <li>DEVELOPER</li>
                                        <li>PHOTOGRAPHER</li>
                                    </ul>
                                </div>
                                <div class="header-social social-bookmark">
                                    <ul>
                                        <li>
                                            <a href="#"><i class="ti-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="ti-instagram"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="ti-tumblr"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="ti-twitter"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slider area end -->
            <!-- About area start -->
            <div class="about-area white-bg pt-110" id="about">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-5">
                            <div class="profile-image">
                                <img src="<?= base_url('assets/user/img/bala_profile.jpg'); ?>" alt="">
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-1 col-lg-6 col-lg-offset-1">
                            <div class="profile-wrapper pt-115">
                                <div class="section-title">
                                    <h1>About Me</h1>
                                </div>
                                <div class="profile-details">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidiei dunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exeracita aation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                </div>
                                <div class="profile-action-btn">
                                    <a class="button" href="#">HIRE ME</a>
                                    <a class="button button-white" href="#">DOWNLOAD CV</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About area end -->
            <!-- Skill area start -->
            <div class="skill-area white-bg pt-100 pb-110">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <h1>My Skills</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="progress-wrapper">
                                <div class="single-progressbar">
                                    <h6>Web Design</h6>
                                    <div class="progress">
                                        <div class="progress-bar wow fadeInLeft animated" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;" data-wow-delay=".5s" data-wow-duration="0.5s">
                                            <span>90%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="single-progressbar">
                                    <h6>Web Development</h6>
                                    <div class="progress">
                                        <div class="progress-bar wow fadeInLeft animated" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%;" data-wow-delay=".6s" data-wow-duration="0.6s">
                                            <span>95%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="progress-wrapper">
                                <div class="single-progressbar">
                                    <h6>Marketing</h6>
                                    <div class="progress">
                                        <div class="progress-bar wow fadeInLeft animated" role="progressbar" aria-valuenow="97" aria-valuemin="0" aria-valuemax="100" style="width: 97%;" data-wow-delay=".7s" data-wow-duration="0.7s">
                                            <span>97%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="single-progressbar">
                                    <h6>SEO & Marketing</h6>
                                    <div class="progress">
                                        <div class="progress-bar wow fadeInLeft animated" role="progressbar" aria-valuenow="92" aria-valuemin="0" aria-valuemax="100" style="width: 92%;" data-wow-delay=".8s" data-wow-duration="0.8s">
                                            <span>92%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Skill area end -->
            <!-- Service area start -->
            <div class="service-area gray-bg pt-110 pb-80" id="service">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <h1>My Services</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="service-slider-wrapper">
                                <div class="service-slider">
                                    <div class="single-service">
                                        <div class="service-content text-center">
                                            <i class="ti-desktop"></i>
                                            <h5>PRODUCT DESIGN</h5>
                                            <p>Lorem ipsum dolor amet, consecte tempor incididunt ut labore et dolore tumber tur adipisicing elit.</p>
                                        </div>
                                    </div>
                                    <div class="single-service">
                                        <div class="service-content text-center">
                                            <i class="ti-vector"></i>
                                            <h5>UI/UX DESIGN</h5>
                                            <p>Lorem ipsum dolor amet, consecte tempor incididunt ut labore et dolore tumber tur adipisicing elit.</p>
                                        </div>
                                    </div>
                                    <div class="single-service">
                                        <div class="service-content text-center">
                                            <i class="ti-paint-bucket"></i>
                                            <h5>APP DEVELOPMENT</h5>
                                            <p>Lorem ipsum dolor amet, consecte tempor incididunt ut labore et dolore tumber tur adipisicing elit.</p>
                                        </div>
                                    </div>
                                    <div class="single-service">
                                        <div class="service-content text-center">
                                            <i class="ti-camera"></i>
                                            <h5>PHOTOGRAPHY</h5>
                                            <p>Lorem ipsum dolor amet, consecte tempor incididunt ut labore et dolore tumber tur adipisicing elit.</p>
                                        </div>
                                    </div>
                                    <div class="single-service">
                                        <div class="service-content text-center">
                                            <i class="ti-desktop"></i>
                                            <h5>PRODUCT DESIGN</h5>
                                            <p>Lorem ipsum dolor amet, consecte tempor incididunt ut labore et dolore tumber tur adipisicing elit.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Service area end -->
            <!-- Portfolio area start -->
            <div class="portfolio-area white-bg pt-110 pb-80" id="portfolio">
                <div class="container">
                    <div class="row">
                        <div class="section-title text-center">
                            <h1>Latest Projects</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="portfolio-content">
                                <div class="portfolio-filter-wrap text-center">
                                    <ul class="portfolio-filter">
                                        <li class="active" data-filter="*">ALL WORK</li>
                                        <li data-filter=".exterior">EXTERIOR</li>
                                        <li data-filter=".interior">INTERIOR</li>
                                        <li data-filter=".logo">BRAND & IDENTITY</li>
                                        <li data-filter=".digital">DIGITAL PAINTINGS</li>
                                        <li data-filter=".pencil">PENCIL SKETCHES</li>
                                    </ul>
                                </div>
                                <div class="portfolio portfolio-masonry">

                                <?php foreach ($projects as $project): ?>
                                    
                                    <div class="portfolio-item <?php echo $project->project_category ?> ">
                                        <div class="portfolio-item-content">
                                            <div class="item-thumbnail">
                                                <img src="<?php echo $project->project_image ?>" alt="">
                                            </div>

                                            
                                            <div class="portfolio-details">
                                                <div class="portfolio-details-inner">
                                                    <a class="venobox portfolio-view-btn" data-gall="myGallery" href="<?php echo $project->project_image ?>"><i class="ti-fullscreen"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Portfolio area end -->

            <!-- Client logo area start -->
            <div class="client-logo-area gray-bg pt-110 pb-120">
                <div class="container">
                    <div class="row">
                        <div class="section-title text-center">
                            <h1>Logo Designs</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="client-logo-slider">
                                <div class="single-brand-logo">
                                    <div class="client-logo-wrapper text-center">
                                        <div class="client-logo-inner">
                                            <img src="<?= base_url('assets/user/img/clients/1.png'); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="single-brand-logo">
                                    <div class="client-logo-wrapper text-center">
                                        <div class="client-logo-inner">
                                            <img src="<?= base_url('assets/user/img/clients/2.png'); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="single-brand-logo">
                                    <div class="client-logo-wrapper text-center">
                                        <div class="client-logo-inner">
                                            <img src="<?= base_url('assets/user/img/clients/3.png'); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="single-brand-logo">
                                    <div class="client-logo-wrapper text-center">
                                        <div class="client-logo-inner">
                                            <img src="<?= base_url('assets/user/img/clients/4.png'); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="single-brand-logo">
                                    <div class="client-logo-wrapper text-center">
                                        <div class="client-logo-inner">
                                            <img src="<?= base_url('assets/user/img/clients/5.png'); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="single-brand-logo">
                                    <div class="client-logo-wrapper text-center">
                                        <div class="client-logo-inner">
                                            <img src="<?= base_url('assets/user/img/clients/6.png'); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="single-brand-logo">
                                    <div class="client-logo-wrapper text-center">
                                        <div class="client-logo-inner">
                                            <img src="<?= base_url('assets/user/img/clients/1.png'); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="single-brand-logo">
                                    <div class="client-logo-wrapper text-center">
                                        <div class="client-logo-inner">
                                            <img src="<?= base_url('assets/user/img/clients/2.png'); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="single-brand-logo">
                                    <div class="client-logo-wrapper text-center">
                                        <div class="client-logo-inner">
                                            <img src="<?= base_url('assets/user/img/clients/2.png'); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="single-brand-logo">
                                    <div class="client-logo-wrapper text-center">
                                        <div class="client-logo-inner">
                                            <img src="<?= base_url('assets/user/img/clients/2.png'); ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Client logo area end -->

            <!-- Resume area start -->
            <div class="resume-area pt-110 pb-80 gray-bg" id="resume">
                <div class="container">
                    <div class="row">
                        <div class="section-title text-center">
                            <h1>Resume</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="resume-wrapper left">
                                <div class="resume-title">
                                    <h2> <span><i class="ti-cup"></i></span>Education:</h2>
                                </div>
                                <div class="single-resume">
                                    <p class="work-duration">March 2013 - Present Deacember.</p>
                                    <h4>Diploma in Information Technology</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmodm incididunt ut labore et dolore magna aliqua.</p>
                                    <h5>Nortsouth University</h5>
                                </div>
                                <div class="single-resume">
                                    <p class="work-duration">March 2011 - 2014 Deacember.</p>
                                    <h4>Professional Web Design</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmodm incididunt ut labore et dolore magna aliqua.</p>
                                    <h5>Florida University</h5>
                                </div>
                                <div class="single-resume">
                                    <p class="work-duration">March 2008 - 2010 Deacember.</p>
                                    <h4>Higher Secondary School Certificate</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmodm incididunt ut labore et dolore magna aliqua.</p>
                                    <h5>University of California</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="resume-wrapper right">
                                <div class="resume-title">
                                    <h2> <span><i class="ti-bag"></i></span>Experience:</h2>
                                </div>
                                <div class="single-resume">
                                    <p class="work-duration">March 1011- Present</p>
                                    <h4><span><img src="<?= base_url('assets/user/img/others/behance.png'); ?>" alt=""></span>Behance</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmodm incididunt ut labore et dolore magna aliqua.</p>
                                    <h5>Senior UI UX Designer</h5>
                                </div>
                                <div class="single-resume">
                                    <p class="work-duration">March 2011- 2014 Deacember.</p>
                                    <h4><span><img src="<?= base_url('assets/user/img/others/envato.png'); ?>" alt=""></span>Envato</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmodm incididunt ut labore et dolore magna aliqua.</p>
                                    <h5>Digital Strategic Planner</h5>
                                </div>
                                <div class="single-resume">
                                    <p class="work-duration">March 2011- 2014 Deacember.</p>
                                    <h4><span><img src="<?= base_url('assets/user/img/others/dribbble.png'); ?>" alt=""></span>Dribbble</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do iusmodm incididunt ut labore et dolore magna aliqua.</p>
                                    <h5>Creative Director</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Resume area end -->
            <!-- Testimonial area start -->
            <div class="testimonial-area white-bg pt-110 pb-150">
                <div class="container">
                    <div class="row">
                        <div class="section-title text-center">
                            <h1>Testimonials</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="testimonial-wrapper">
                                <div class="testimonial-slider">
                                    <div class="single-testimonial">
                                        <div class="client-avatar">
                                            <img src="<?= base_url('assets/user/img/avatar/1.png'); ?>" alt="">
                                        </div>
                                        <div class="testimonial-content-wrapper">
                                            <div class="testimonial-content text-center">
                                                <h4>Jeremy Mathiue</h4>
                                                <h6>ECO marveltheme</h6>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incubtconsectetur aliqua. Ultra enim ad minim veniam, quis nostrud exercitatio ullamgco laboris nisi ut com.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-testimonial">
                                        <div class="client-avatar">
                                            <img src="<?= base_url('assets/user/img/avatar/2.png'); ?>" alt="">
                                        </div>
                                        <div class="testimonial-content-wrapper">
                                            <div class="testimonial-content text-center">
                                                <h4>Nancy Franklin</h4>
                                                <h6>Creative Director</h6>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incubtconsectetur aliqua. Ultra enim ad minim veniam, quis nostrud exercitatio ullamgco laboris nisi ut com.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-testimonial">
                                        <div class="client-avatar">
                                            <img src="<?= base_url('assets/user/img/avatar/1.png'); ?>" alt="">
                                        </div>
                                        <div class="testimonial-content-wrapper">
                                            <div class="testimonial-content text-center">
                                                <h4>Jeremy Mathiue</h4>
                                                <h6>ECO marveltheme</h6>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incubtconsectetur aliqua. Ultra enim ad minim veniam, quis nostrud exercitatio ullamgco laboris nisi ut com.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-testimonial">
                                        <div class="client-avatar">
                                            <img src="<?= base_url('assets/user/img/avatar/2.png'); ?>" alt="">
                                        </div>
                                        <div class="testimonial-content-wrapper">
                                            <div class="testimonial-content text-center">
                                                <h4>Nancy Franklin</h4>
                                                <h6>Creative Director</h6>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incubtconsectetur aliqua. Ultra enim ad minim veniam, quis nostrud exercitatio ullamgco laboris nisi ut com.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-testimonial">
                                        <div class="client-avatar">
                                            <img src="<?= base_url('assets/user/img/avatar/1.png'); ?>" alt="">
                                        </div>
                                        <div class="testimonial-content-wrapper">
                                            <div class="testimonial-content text-center">
                                                <h4>Jeremy Mathiue</h4>
                                                <h6>ECO marveltheme</h6>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incubtconsectetur aliqua. Ultra enim ad minim veniam, quis nostrud exercitatio ullamgco laboris nisi ut com.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-testimonial">
                                        <div class="client-avatar">
                                            <img src="<?= base_url('assets/user/img/avatar/2.png'); ?>" alt="">
                                        </div>
                                        <div class="testimonial-content-wrapper">
                                            <div class="testimonial-content text-center">
                                                <h4>Nancy Franklin</h4>
                                                <h6>Creative Director</h6>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed eiusmod tempor incubtconsectetur aliqua. Ultra enim ad minim veniam, quis nostrud exercitatio ullamgco laboris nisi ut com.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Testimonial area end -->
            <!-- Counter up area start -->
            <div class="counterup-area pb-100 white-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="counter-wrapper">
                                <div class="single-counter">
                                    <div class="counter-content">
                                        <i class="ti-cup"></i>
                                        <h3>Cup of Tea</h3>
                                        <h2 class="counter-number">1200</h2>
                                    </div>
                                </div>
                                <div class="single-counter">
                                    <div class="counter-content">
                                        <i class="ti-heart"></i>
                                        <h3>Happy Client</h3>
                                        <h2 class="counter-number">1080</h2>
                                    </div>
                                </div>
                                <div class="single-counter">
                                    <div class="counter-content">
                                        <i class="ti-briefcase"></i>
                                        <h3>Project Complete</h3>
                                        <h2 class="counter-number">1170</h2>
                                    </div>
                                </div>
                                <div class="single-counter">
                                    <div class="counter-content">
                                        <i class="ti-medall-alt"></i>
                                        <h3>Awards Win</h3>
                                        <h2 class="counter-number">750</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Counter up area end -->
            
            <!-- Call to action start -->
            <div class="call-to-action gray-bg pt-100 pb-110" id="contact">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="call-to-text text-center">
                                <h1>Wanna Start Work with me?</h1>
                                <p>Tell me about your project story and project brief</p>
                                <a class="button large" href="#">Start Project</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Call to action end -->
            <!-- Contact area start -->
            <div class="contact-area gray-bg pb-110">
                <div class="container">
                    <div class="contact-wrapper">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="contact-address text-center">
                                    <i class="ti-headphone-alt"></i>
                                    <h4>
                                        +91 9175148498
                                    </h4>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="contact-address text-center">
                                    <i class="ti-map-alt"></i>
                                    <h4>
                                        Aashram Road, Urli Kanchan <br>
                                        Pune - 412202
                                    </h4>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="contact-address text-center">
                                    <i class="ti-email"></i>
                                    <h4>
                                        <a href="#">www.iambalaji.com</a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- <form id="contact-us" action="home/store_message" method="POST"> -->
                            <?php echo form_open('home/store_message', ['id'=>'contact-us']); ?>
                                <div class="form-inner">
                                    <div class="col-md-5 col-sm-6">
                                        <div class="input-box">
                                            <input name="name" placeholder="Your Name" type="text">
                                            <input name="email" placeholder="Your Email" type="email">
                                            <input name="mobile" placeholder="Your Mobile No" type="number">
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-6">
                                        <div class="message-box">
                                            <textarea name="message" cols="30" rows="2" placeholder="Project Description"></textarea>
                                            <!-- <input class="button" type="submit" value="SEND MESSAGE"> -->
                                            <?php echo form_submit(['name'=>'submit', 'class'=>'button', 'value'=>'SEND MESSAGE']); ?>
                                            
                                                <p class="contact-send-message">
                                                <?php if($this->session->flashdata('msg')): ?>
                                                <?php echo $this->session->flashdata('msg'); ?>
                                                <?php endif; ?>
                                                </p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Contact area end -->





<?php include('layout/footer.php'); ?>