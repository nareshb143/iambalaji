<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Balaji Battulwar | 3D Visualizer</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon icon -->
    <?= link_tag('assets/user/img/favicon.png', 'icon', 'image/png'); ?>
    <!-- All CSS file included here -->
    <?= link_tag('assets/user/css/bootstrap.min.css'); ?>
    <?= link_tag('assets/user/css/slick.css'); ?>
    <?= link_tag('assets/user/css/elements.css'); ?>
    <?= link_tag('assets/user/css/style.css'); ?>
    <?= link_tag('assets/user/css/responsive.css'); ?>
    <?= link_tag('assets/user/css/themeify-icons.css'); ?>
    <!-- Modernizr JS -->
    <script src="<?= base_url('assets/user/js/vendor/modernizr-2.8.3.min.js'); ?>"></script>
    <script src="<?= base_url('assets/user/js/jquery-3.2.1.min.js'); ?>"></script>

</head>

<body id="top">
    <!-- Main wrapper start -->
    <div class="wrapper">
        <!-- Header area start -->
        <header id="header-area" class="header-area" data-spy="affix" data-offset-top="1">
            <nav class="navbar">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="navbar-inner">
                                <div class="logo">
                                    <a href="#"><img src="<?= base_url('assets/user/img/iamb.png');?>" alt="" /></a>
                                </div>
                                <div class="main-menu-wrapper" id="navigation">
                                    <ul class="nav navbar-nav navbar-right" id="main-menu">
                                        <li class="active"><a href="#home">Home</a></li>
                                        <li><a href="#about">About</a></li>
                                        <li><a href="#service">Service</a></li>
                                        <li><a href="#portfolio">Portfolio</a></li>
                                        <li><a href="#resume">Resume</a></li>
                                        <li><a href="#contact">Contact</a></li>
                                    </ul>
                                </div>
                                <div class="expand-menu-open">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
        <!-- Header area end -->
        <!-- Slide menu start -->
        <div class="mainmenu-expand">
            <div class="close-menu">
                <i class="ti-close"></i>
            </div>
            <div class="menu-logo">
                <a href="index.html"><img src="<?= base_url('assets/user/img/iamb.png');?>" alt=""></a>
            </div>
            <nav class="navbar">
                <div class="mainmenu-box" id="navigation-2">
                    <ul class="nav navbar-nav" id="slide-nav">
                        <li class="active"><a href="#home">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#service">Service</a></li>
                        <li><a href="#portfolio">Portfolio</a></li>
                        <li><a href="#resume">Resume</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </div>
            </nav>
            <div class="sidebar-bookmark-area">
                <p>Flow me on social media:</p>
                <ul class="expand-social social-bookmark">
                    <li>
                        <a href="#"><i class="ti-facebook"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="ti-instagram"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="ti-tumblr"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="ti-twitter"></i></a>
                    </li>
                </ul>
            </div>
            <p class="expand-copyright">Copyright @ 2018 <a href="#">Iambalaji</a> all right reserved.</p>
        </div>
        <!-- Slide menu end -->
        
        