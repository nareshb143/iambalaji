<!-- Footer area start -->
            <footer class="footer-area gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="social-bookmark footer-social text-center">
                                <ul>
                                    <li>
                                        <a href="#"><i class="ti-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="ti-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="ti-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="ti-tumblr"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="copyright text-center">
                                <p>Copyright @ 2018 <a href="#">Iambalaji</a> all right reserved, designed by <a href="#">Balaji B.</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- Footer area end -->
        </div>
        <!-- Content area end  -->
    </div>
    <!-- Main wrapper end -->

<!--     <script type="text/javascript">
        $(document).ready(function(){
            $('.interior').addClass('3d');
            $('.exterior').addClass('3d');
        });
    </script> -->

    <!-- All JS file included here -->
    <script src="<?= base_url('assets/user/js/vendor/jquery-1.12.0.min.js'); ?>"></script>
    <script src="<?= base_url('assets/user/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/user/js/jquery-ui.js'); ?>"></script>
    <script src="<?= base_url('assets/user/js/waypoints.min.js'); ?>"></script>
    <script src="<?= base_url('assets/user/js/jquery.counterup.min.js'); ?>"></script>
    <script src="<?= base_url('assets/user/js/isotope.pkgd.min.js'); ?>"></script>
    <script src="<?= base_url('assets/user/js/slick.min.js'); ?>"></script>
    <script src="<?= base_url('assets/user/js/ajax-mail.js'); ?>"></script>
    <script src="<?= base_url('assets/user/js/plugins.js'); ?>"></script>
    <script src="<?= base_url('assets/user/js/main.js'); ?>"></script>
    
</body>


</html>